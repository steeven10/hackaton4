from django.db import models

# Create your models here.
class Speciality(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    date_of_register = models.DateField()
    date_of_modification =models.DateField()
    register_user=models.CharField(max_length=50)
    modified_user=models.CharField(max_length=50)
    active = models.BooleanField()
    def __str__(self):
        return self.name

class Medical(models.Model):
    sexs = [
        ('male','Male'),
        ('female','Female'),
        ('NA','NA')
    ]
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.IntegerField()
    adress = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.IntegerField()
    sex = models.CharField(max_length=50, choices=sexs)
    num_institution = models.IntegerField()
    date_of_birth = models.DateField()
    date_of_register = models.DateField()
    date_of_modification = models.DateField()
    register_user = models.CharField(max_length=50)
    modified_user = models.CharField(max_length=50)
    active = models.BooleanField()
    def __str__(self):
        return self.first_name + '' + self.last_name

class Patient(models.Model):
    sexs = [
        ('male','Male'),
        ('female','Female'),
        ('NA','NA')
    ]
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.IntegerField()
    adress = models.CharField(max_length=100)
    phone = models.IntegerField()
    sex = models.CharField(max_length=50, choices=sexs)
    date_of_birth = models.DateField()
    date_of_register = models.DateField()
    date_of_modification = models.DateField()
    register_user = models.CharField(max_length=50)
    modified_user = models.CharField(max_length=50)
    active = models.BooleanField()
    def __str__(self):
        return self.first_name + '' + self.last_name

class Medical_speciality(models.Model):
    medical_id = models.ForeignKey(Medical,on_delete=models.CASCADE,related_name="Medical")
    speciality_id = models.ForeignKey(Speciality,on_delete=models.CASCADE)
    date_of_register = models.DateField()
    date_of_modification = models.DateField()
    register_user = models.CharField(max_length=50)
    modified_user = models.CharField(max_length=50)
    active = models.BooleanField()
    def __str__(self):
        return (self.medical_id + '' + self.speciality_id)

class Schedule(models.Model):
    medical_id = models.ForeignKey(Medical, on_delete=models.CASCADE)
    date_of_attention = models.DateField()
    init_of_attention = models.DateTimeField()
    end_of_attention = models.DateTimeField()
    date_of_register = models.DateField()
    register_user = models.CharField(max_length=50)
    date_of_modification = models.DateField()
    modified_user = models.CharField(max_length=50)
    def __str__(self):
        return (self.medical_id)

class Date(models.Model):
    medical_id = models.ForeignKey(Medical, on_delete=models.CASCADE)
    patient_id = models.ForeignKey(Patient, on_delete=models.CASCADE)
    date_of_attention = models.DateField()
    init_of_attention = models.DateTimeField()
    end_of_attention = models.DateTimeField()
    state = models.BooleanField()
    observation = models.TextField()
    active = models.BooleanField()
    date_of_register = models.DateField()
    date_of_modification = models.DateField()
    register_user = models.CharField(max_length=50)
    modified_user = models.CharField(max_length=50)
    def __str__(self):
        return self.date_of_attention
