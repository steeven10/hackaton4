from django.contrib import admin
from .models import Patient,Speciality,Medical,Medical_speciality,Schedule,Date
# Register your models here.
admin.site.register(Patient)
admin.site.register(Medical)
admin.site.register(Speciality)
admin.site.register(Medical_speciality)
admin.site.register(Date)
admin.site.register(Schedule)
