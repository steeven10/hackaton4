from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from rest_framework import serializers

from hospital.models import Patient,Speciality,Medical,Medical_speciality,Schedule,Date
from django.contrib.auth.models import User, AbstractUser
class UserSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff', 'password')
    owner = serializers.ReadOnlyField(source='owner.username')

class SpecialitySerializer(ModelSerializer):
    class Meta:
        model = Speciality
        fields = '__all__'

class MedicalSerializer(ModelSerializer):
    class Meta:
        model = Medical
        fields = '__all__'

class PatientSerializer(ModelSerializer):
    class Meta:
        model = Patient
        fields = '__all__'
class Medical_specialitySerializer(ModelSerializer):
    class Meta:
        model = Medical_speciality
        fields = '__all__'

class ScheduleSerializer(ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'

class DateSerializer(ModelSerializer):
    class Meta:
        model = Date
        fields = '__all__'