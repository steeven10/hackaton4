from django.shortcuts import render
from django.contrib.auth.models import User
from hospital.models import Patient,Medical,Medical_speciality,Schedule,Speciality,Date
from api.serializer import UserSerializer,PatientSerializer,MedicalSerializer,Medical_specialitySerializer,SpecialitySerializer,ScheduleSerializer,DateSerializer
from rest_framework import viewsets

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class PatientViewSet(viewsets.ModelViewSet):
    serializer_class = PatientSerializer
    queryset = Patient.objects.all()

class MedicalViewSet(viewsets.ModelViewSet):
    serializer_class = MedicalSerializer
    queryset = Medical.objects.all()

class Medical_specialityViewSet(viewsets.ModelViewSet):
    serializer_class = Medical_specialitySerializer
    queryset = Medical_speciality.objects.all()

class SpecialityViewSet(viewsets.ModelViewSet):
    serializer_class = SpecialitySerializer
    queryset = Speciality.objects.all()

class ScheduleViewSet(viewsets.ModelViewSet):
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

class DateViewSet(viewsets.ModelViewSet):
    serializer_class = DateSerializer
    queryset = Date.objects.all()