from rest_framework import routers
from api.views import UserViewSet,PatientViewSet,MedicalViewSet,Medical_specialityViewSet,ScheduleViewSet,SpecialityViewSet,DateViewSet

router = routers.DefaultRouter()
router.register(r'user',UserViewSet)
router.register(r'patient',PatientViewSet)
router.register(r'medical',MedicalViewSet)
router.register(r'medical_speciality',Medical_specialityViewSet)
router.register(r'speciality',SpecialityViewSet)
router.register(r'schedule',ScheduleViewSet)
router.register(r'date',DateViewSet)
