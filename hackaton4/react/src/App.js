
import './App.css';
import { Routes, Route} from "react-router-dom";
import {Header} from './componentes/header'
import {Home} from "./componentes/home";
import {Patient_list} from "./componentes/patient/patient";
import{Medical_list} from "./componentes/medical/medical"
import { Speciality_list } from './componentes/speciality/speciality';
import{Medical_speciality_list} from './componentes/medical_speciality/medical_speciality'
import{Patient_form} from './componentes/patient/patient_form'
import{MedicalSpeciality_form} from './componentes/medical_speciality/medical_speciality_form'
import { Medical_form } from './componentes/medical/medical_form';

function App() {
    return (
        <div className="App">
            <Header/>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/patient" element={<Patient_list/>}/>
                <Route path="/patient_form" element={<Patient_form/>}/>
                <Route path="/updatePatient/:id" element={<Patient_form/>}/>
                <Route path="/medical" element={<Medical_list/>}/>
                <Route path="/medical_form" element={<Medical_form/>}/>
                <Route path="/updateMedical/:id" element={<Medical_form/>}/>
                <Route path="/speciality" element={<Speciality_list/>}/>
                <Route path="/medical_speciality" element={<Medical_speciality_list/>}/>
                <Route path="/medical_speciality_form" element={<MedicalSpeciality_form/>}/>
                <Route path="/updateMedicalSpeciality/:id" element={<MedicalSpeciality_form/>}/>
            </Routes>
        </div>
    );
}

export default App;
