

import { useEffect, useState } from "react";
import axios from "axios";
import { MedicaltItem } from "./medical_item";

export function Medical_list () {
    const [medical,setMedical] = useState(null)

    const listMedical = async (req, res) => {
        axios.get('http://127.0.0.1:8000/api/medical/?format=json').then((respuesta)=>{
            setMedical(respuesta.data)
        }
        )
    }

    useEffect(() =>{
        listMedical();

    },[])
    return (
    <div className="row">
        {medical ? medical.results.map((elemento)=>{
        return <MedicaltItem
        key = {elemento.id}
        {...elemento}
        listMedical={listMedical}/>}) : "patients..."}
    </div>
    )
}