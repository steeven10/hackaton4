import {useState,useEffect} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Medical_server from './medical_server'
export function Medical_form () {
    const navigate = useNavigate();
    const params=useParams();
    const initialState = {first_name:'',last_name:'',email:'',num_institution:'',dni:0,adress:'',phone:'',sex:'NA',date_of_birth:"2000-01-01",date_of_register:"2000-01-01",date_of_modification:"2000-01-01",register_user:'',modified_user:'',active:false};
    const sexo = [{sex:'male'},{sex:'female'},{sex:'NA'}]
    const [medical,setMedical]= useState(initialState)
    const handleInputChange=(e)=>{
        setMedical({...medical,[e.target.name]:e.target.value});
    }
    const handleSubmit = async (e)=>{
        e.preventDefault();
        try{
            let res;
            if(!params.id){            res =await Medical_server.registerMedical(medical);
              const data = await res.json();
              if (data.message === 'Success'){
                setMedical({initialState})
              }}else{
                await Medical_server.updateMedical(params.id,medical)
              }

            navigate("../medical", { replace: true });
        }catch(error){
            console.log(error);
        }
    }
    console.log(medical)
    const getMedical = async (medicalid) => {
      try{
        const res=await Medical_server.getMedical(medicalid);
        const data = await res.json();
        console.log(data)
        const {first_name,last_name,dni,adress,phone,email,num_institution,sex,date_of_birth,date_of_register,date_of_modification,register_user,modified_user,active}=data;
        setMedical({first_name,last_name,dni,adress,email,num_institution,phone,sex,date_of_birth,date_of_register,date_of_modification,register_user,modified_user,active})
      }catch(error){
        console.log(error);
      }
    }
    useEffect(()=>{
      if(params.id){
        getMedical(params.id)
      }
      //eslint-disable-next-line
    },[]);
    return(   <div className="container">
      <h1 className="text-center my-3">Medical form</h1>
        <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">First name</label>
          <input type="text" name = "first_name" value ={medical.first_name} onChange={handleInputChange}  className="form-control" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">Last name</label>
          <input type="text" name = "last_name" value = {medical.last_name} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">DNI</label>
          <input type="number" name="dni" value = {medical.dni} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">Adress</label>
          <input type="text" name="adress" value = {medical.adress} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">Phone</label>
          <input type="number" name="phone" value = {medical.phone} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">Email</label>
          <input type="email" name="email" value = {medical.email} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">Number institution</label>
          <input type="number" name="num_institution" value = {medical.num_institution} onChange={handleInputChange} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <select className="form-select"  name="sex" aria-label="Default select example" value={medical.sex} onChange={handleInputChange}>
          {sexo.map(sex => <option  key={sex.sex} value={sex.sex} >{sex.sex}</option>)}
        </select>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Date of birth</label>
          <input type="date" name="date_of_birth" value = {medical.date_of_birth} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Date of register</label>
          <input type="date" name="date_of_register" value = {medical.date_of_register} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Date of modification</label>
          <input type="date" name="date_of_modification" pattern="\d{2}-\d{2}-\d{4}" value = {medical.date_of_modification} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Register user</label>
          <input type="text" name="register_user" value = {medical.register_user} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Modified user</label>
          <input type="text" name="modified_user" value = {medical.modified_user} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3 form-check">
          <input type="checkbox" name="active"  className="form-check-input" id="exampleCheck1" onChange={handleInputChange}/>
          <label className="form-check-label" for="exampleCheck1">Active</label>
        </div>
        {params.id?(<button type="submit" className="btn btn-primary">Update</button>):(<button type="submit" className="btn btn-primary">Submit</button>)}
        
      </form>
    </div> )

}