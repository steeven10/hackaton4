
export const registerMedical = async (mewMedical)=>{
    return await fetch('http://127.0.0.1:8000/api/medical/?format=json',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "first_name":String(mewMedical.first_name).trim(),
            "last_name":String(mewMedical.last_name).trim(),
            "dni":parseInt(mewMedical.dni),
            "adress":String(mewMedical.adress).trim(),
            "phone":parseInt(mewMedical.phone),
            "num_institution":parseInt(mewMedical.num_institution),
            "email":String(mewMedical.email).trim(),
            "sex":String(mewMedical.sex).trim(),
            "date_of_birth":String(mewMedical.date_of_birth),
            "date_of_register":String(mewMedical.date_of_register),
            "date_of_modification":String(mewMedical.date_of_modification),
            "register_user":String(mewMedical.register_user).trim(),
            "modified_user":String(mewMedical.modified_user).trim(),
            "active":String(mewMedical.active)
        })
    })
};
export const updateMedical = async (medicalid,mewMedical)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical/${medicalid}/?format=json`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "first_name":String(mewMedical.first_name).trim(),
            "last_name":String(mewMedical.last_name).trim(),
            "dni":parseInt(mewMedical.dni),
            "adress":String(mewMedical.adress).trim(),
            "phone":parseInt(mewMedical.phone),
            "num_institution":parseInt(mewMedical.num_institution),
            "email":String(mewMedical.email).trim(),
            "sex":String(mewMedical.sex).trim(),
            "date_of_birth":String(mewMedical.date_of_birth),
            "date_of_register":String(mewMedical.date_of_register),
            "date_of_modification":String(mewMedical.date_of_modification),
            "register_user":String(mewMedical.register_user).trim(),
            "modified_user":String(mewMedical.modified_user).trim(),
            "active":String(mewMedical.active)
        })
    })
};

export const getMedical = async (medicalid)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical/${medicalid}/?format=json`)
};

export const deleteMedical = async (medicalid)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical/${medicalid}/?format=json`,{
        method: 'DELETE',
    })
};