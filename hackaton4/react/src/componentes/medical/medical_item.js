import {Link} from "react-router-dom"
import {useNavigate} from "react-router-dom"
import * as Medical_server from './medical_server'
function colorEstiloCirculo(color = 'green') {
    const estiloDelCirculo = {
        width: '10px',
        height: '10px',
        display: "inline-block",
        backgroundColor: color,
        borderRadius: '50%',
        marginRight: "5px"
    }
    return estiloDelCirculo
}

export function MedicaltItem({
                                   id,
                                   first_name,
                                   last_name,
                                   dni,
                                   adress,
                                   phone,
                                   sex,
                                   num_institution,
                                   date_of_birth,
                                   date_of_register,
                                   date_of_modification,
                                   register_user,
                                   modified_user,
                                   active,
                                   listMedical
                               }) {
    let color = 'green';
    if (active === true) {
        color = 'green'
    }
    if (active === false) {
        color = 'red'
    }
    const handleDelete= async(patientid)=>{
        await Medical_server.deleteMedical(patientid)
        listMedical();
    }
    const navigate = useNavigate();
    return (
        <div className="mx-5">
            <h1 className="text-center my-3">Medical</h1>
            <div className="py-3 col-4 ">
                <div className="card mb-3" style={{maxWidth: "540px"}}>
                    <div className="row g-0 nabvar-primary bg-dark" >
                        <div className="col-md-10 ">
                            <div className="card-body">
                                <h5 className="card-title mb-0"style={{color: 'white'}} >{first_name} {last_name}</h5>
                                <p style={{color: 'white'}}><span style={colorEstiloCirculo(color)}
                                ></span>
                                    {active}  {dni}
                                </p>
                                <p className="card-text mb-0"><small className="text-muted">Adress</small></p>
                                <p style={{color: 'white'}}> {adress}</p >
                                <p className="card-text mb-0"><small className="text-muted">Phone</small></p>
                                <p style={{color: 'white'}}> {phone}</p>
                                <p className="card-text mb-0"><small className="text-muted">Sex</small></p>
                                <p style={{color: 'white'}}> {sex}</p>
                                <p className="card-text mb-0"><small className="text-muted">Num institution</small></p>
                                <p style={{color: 'white'}}> {num_institution}</p>
                                <p className="card-text mb-0"><small className="text-muted">Date of birth</small></p>
                                <p style={{color: 'white'}}> {date_of_birth}</p>
                                <p className="card-text mb-0"><small className="text-muted">Date of register</small></p>
                                <p style={{color: 'white'}}> {date_of_register}</p>
                                <p className="card-text mb-0"><small className="text-muted">Date of modification</small>
                                </p>
                                <p style={{color: 'white'}}> {date_of_modification}</p>
                                <p className="card-text mb-0"><small className="text-muted">User register</small></p>
                                <p style={{color: 'white'}}> {register_user}</p>
                                <p className="card-text mb-0"><small className="text-muted">User modified</small></p>
                                <p style={{color: 'white'}}> {modified_user}</p>
                                <div className="row">
                                    <div className="col-6">
                                        <button onClick={()=>id && handleDelete(id)} className="btn btn-danger my-2" >delete</button>
                                    </div>
                                    <div className="col-6">
                                        <button onClick={()=>id && navigate(`../updateMedical/${id}`, { replace: true })} className="btn btn-light my-2" >update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        </div>
        

    )
}