

import { useEffect, useState } from "react";
import axios from "axios";
import { MedicalSpecialityItem } from "./medical_speciality_item";

export function Medical_speciality_list () {
    const [medical_speciality,setMedical_speciality] = useState(null)

    const listMedicalSpeciality = async (req, res) => {
        axios.get('http://127.0.0.1:8000/api/medical_speciality/?format=json').then((respuesta)=>{
            setMedical_speciality(respuesta.data)
        }
        )
    }

    useEffect(() =>{
        listMedicalSpeciality();
    },[])
    return (
    <div className="container">
        <h1 className="text-center my-3">Medical Speciality</h1>
        <div className="row">
        {medical_speciality ? medical_speciality.results.map((elemento)=>{
        return <MedicalSpecialityItem
        key = {elemento.id}
        {...elemento}
        listMedicalSpeciality={listMedicalSpeciality}/>}) : "medical_speciality..."}
        </div>

    </div>
    )
}