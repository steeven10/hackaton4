
export const registerMedicalSpeciality = async (mewMedicalSpeciality)=>{
    return await fetch('http://127.0.0.1:8000/api/medical_speciality/?format=json',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "medical_id":parseInt(mewMedicalSpeciality.medical_id),
            "speciality_id":String(mewMedicalSpeciality.speciality_id),
            "date_of_register":String(mewMedicalSpeciality.date_of_register),
            "date_of_modification":String(mewMedicalSpeciality.date_of_modification),
            "register_user":String(mewMedicalSpeciality.register_user).trim(),
            "modified_user":String(mewMedicalSpeciality.modified_user).trim(),
            "active":String(mewMedicalSpeciality.active)
        })
    })
};
export const updateMedicalSpeciality = async (MedicalSpecialityid,mewMedicalSpeciality)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical_speciality/${MedicalSpecialityid}/?format=json`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "medical_id":parseInt(mewMedicalSpeciality.medical_id),
            "speciality_id":String(mewMedicalSpeciality.speciality_id),
            "date_of_register":String(mewMedicalSpeciality.date_of_register),
            "date_of_modification":String(mewMedicalSpeciality.date_of_modification),
            "register_user":String(mewMedicalSpeciality.register_user).trim(),
            "modified_user":String(mewMedicalSpeciality.modified_user).trim(),
            "active":String(mewMedicalSpeciality.active)
        })
    })
};

export const getMedicalSpeciality = async (MedicalSpecialityid)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical_speciality/${MedicalSpecialityid}/?format=json`)
};

export const deleteMedicalSpeciality = async (MedicalSpecialityid)=>{
    return await fetch(`http://127.0.0.1:8000/api/medical_speciality/${MedicalSpecialityid}/?format=json`,{
        method: 'DELETE',
    })
};