import {useState,useEffect} from 'react';
import axios from "axios";
import { useNavigate, useParams } from 'react-router-dom';
import * as MedicalSpeciality_server from './medical_speciality_server'
export function MedicalSpeciality_form () {

    const navigate = useNavigate();
    const params=useParams();
    const initialState = {medical_id:1,speciality_id:1,date_of_register:"2000-01-01",date_of_modification:"2000-01-01",register_user:'',modified_user:'',active:false};
   
    const [medical_speciality,setMedical_speciality]= useState(initialState)
    const handleInputChange=(e)=>{
        setMedical_speciality({...medical_speciality,[e.target.name]:e.target.value});
    }
    const [medical,setMedical] = useState(null)

    useEffect(() =>{
        axios.get(`http://127.0.0.1:8000/api/medical/?format=json`).then((respuesta)=>{
            setMedical(respuesta.data)
        }
        )
        
    },[])

    const [speciality,setSpeciality] = useState(null)

    useEffect(() =>{
        axios.get(`http://127.0.0.1:8000/api/speciality/?format=json`).then((respuesta)=>{
            setSpeciality(respuesta.data)
        }
        )
        
    },[])
    
    const handleSubmit = async (e)=>{
        e.preventDefault();
        try{
            let res;
            if(!params.id){            res =await MedicalSpeciality_server.registerMedicalSpeciality(medical_speciality);
              const data = await res.json();
              if (data.message === 'Success'){
                setMedical_speciality({initialState})
              }}else{
                await MedicalSpeciality_server.updateMedicalSpeciality(params.id,medical_speciality)
              }

            navigate("../medical_speciality", { replace: true });
        }catch(error){
            console.log(error);
        }
    }

    const getPatient = async (patientid) => {
      try{
        const res=await MedicalSpeciality_server.getMedicalSpeciality(patientid);
        const data = await res.json();
        console.log(data)
        const {medical_id,
          speciality_id,
          date_of_register,
          date_of_modification,
          register_user,
          modified_user,
          active,}=data;
        setMedical_speciality({
          medical_id,
          speciality_id,
          date_of_register,
          date_of_modification,
          register_user,
          modified_user,
          active,})
      }catch(error){
        console.log(error);
      }
    }
    useEffect(()=>{
      if(params.id){
        getPatient(params.id)
      }
      //eslint-disable-next-line
    },[]);
    console.log(medical_speciality)
    if(medical && speciality){
    const med = medical.results
    const spe = speciality.results
    return(   <div className="container">
      <h1 className="text-center my-3">Medical Speciality form</h1>
        <form onSubmit={handleSubmit}>
        <label for="exampleInputPassword1" className="form-label">Medical</label>
        <select className="form-select"  name="medical_id" aria-label="Default select example" value={medical_speciality.medical_id} onChange={handleInputChange}>
          {med.map(med => <option  key={med.id} value={med.id} >{med.first_name} {med.last_name}</option>)}
        </select>
        <label for="exampleInputPassword1" className="form-label">Speciality</label>
        <select className="form-select"  name="speciality_id" aria-label="Default select example" value={medical_speciality.medical_id} onChange={handleInputChange}>
          {spe.map(spe => <option  key={spe.id} value={spe.id} >{spe.name}</option>)}
        </select>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Date of register</label>
          <input type="date" name="date_of_register" value = {medical_speciality.date_of_register} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Date of modification</label>
          <input type="date" name="date_of_modification" pattern="\d{2}-\d{2}-\d{4}" value = {medical_speciality.date_of_modification} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Register user</label>
          <input type="text" name="register_user" value = {medical_speciality.register_user} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3">
          <label for="exampleInputPassword1" className="form-label">Modified user</label>
          <input type="text" name="modified_user" value = {medical_speciality.modified_user} onChange={handleInputChange} className="form-control" id="exampleInputPassword1"/>
        </div>
        <div className="mb-3 form-check">
          <input type="checkbox" name="active"  className="form-check-input" id="exampleCheck1" onChange={handleInputChange}/>
          <label className="form-check-label" for="exampleCheck1">Active</label>
        </div>
        {params.id?(<button type="submit" className="btn btn-primary">Update</button>):(<button type="submit" className="btn btn-primary">Submit</button>)}
        
      </form>
    </div> )
    }
}