import {Link} from "react-router-dom"
import {useEffect, useState} from "react"
import axios from "axios";
import * as MedicalSpeciality_server from './medical_speciality_server'
import {useNavigate} from "react-router-dom"
function colorEstiloCirculo(color = 'green') {
    const estiloDelCirculo = {
        width: '10px',
        height: '10px',
        display: "inline-block",
        backgroundColor: color,
        borderRadius: '50%',
        marginRight: "5px"
    }
    return estiloDelCirculo
}

export function MedicalSpecialityItem({
                                   id,
                                   medical_id,
                                   speciality_id,
                                   date_of_register,
                                   date_of_modification,
                                   register_user,
                                   modified_user,
                                   active,
                                   listMedicalSpeciality
                               }) {
    let color = 'green';
    if (active === true) {
        color = 'green'
    }
    if (active === false) {
        color = 'red'
    }
    const [medical,setMedical] = useState(null)

    useEffect(() =>{
        axios.get(`http://127.0.0.1:8000/api/medical/${medical_id}/?format=json`).then((respuesta)=>{
            setMedical(respuesta.data)
        }
        )
        
    },[])

    const [speciality,setSpeciality] = useState(null)

    useEffect(() =>{
        axios.get(`http://127.0.0.1:8000/api/speciality/${speciality_id}/?format=json`).then((respuesta)=>{
            setSpeciality(respuesta.data)
        }
        )
        
    },[])
    let medico, especialidad
    const handleDelete= async(patientid)=>{
        await MedicalSpeciality_server.deleteMedicalSpeciality(patientid)
        listMedicalSpeciality();
    }
    const navigate = useNavigate();
   if (medical && speciality){
       medico = medical
       especialidad = speciality
    
    
   
    return (
        <div className="col-4">
            <div className="py-3">
                <div className="card mb-3" style={{maxWidth: "540px"}}>
                    <div className="row g-0 nabvar-primary bg-dark" >
                        <div className="col-md-10 ">
                            <div className="card-body">
                                <p style={{color: 'white'}}><span style={colorEstiloCirculo(color)}
                                ></span>
                                    {active} {medico.first_name} {medico.last_name} {especialidad.name}
                                </p>
                                <p className="card-text mb-0"><small className="text-muted">Date of register</small></p>
                                <p style={{color: 'white'}}> {date_of_register}</p>
                                <p className="card-text mb-0"><small className="text-muted">Date of modification</small>
                                </p>
                                <p style={{color: 'white'}}> {date_of_modification}</p>
                                <p className="card-text mb-0"><small className="text-muted">User register</small></p>
                                <p style={{color: 'white'}}> {register_user}</p>
                                <p className="card-text mb-0"><small className="text-muted">User modified</small></p>
                                <p style={{color: 'white'}}> {modified_user}</p>
                                <div className="row">
                                    <div className="col-6">
                                        <button onClick={()=>id && handleDelete(id)} className="btn btn-danger my-2" >delete</button>
                                    </div>
                                    <div className="col-6">
                                        <button onClick={()=>id && navigate(`../updateMedicalSpeciality/${id}`, { replace: true })} className="btn btn-light my-2" >update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        </div>
        

    )
   }
}