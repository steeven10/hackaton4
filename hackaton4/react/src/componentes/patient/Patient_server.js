
export const registerPatient = async (mewPatient)=>{
    return await fetch('http://127.0.0.1:8000/api/patient/?format=json',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "first_name":String(mewPatient.first_name).trim(),
            "last_name":String(mewPatient.last_name).trim(),
            "dni":parseInt(mewPatient.dni),
            "adress":String(mewPatient.adress).trim(),
            "phone":parseInt(mewPatient.phone),
            "sex":String(mewPatient.sex).trim(),
            "date_of_birth":String(mewPatient.date_of_birth),
            "date_of_register":String(mewPatient.date_of_register),
            "date_of_modification":String(mewPatient.date_of_modification),
            "register_user":String(mewPatient.register_user).trim(),
            "modified_user":String(mewPatient.modified_user).trim(),
            "active":String(mewPatient.active)
        })
    })
};
export const updatePatient = async (patientid,mewPatient)=>{
    return await fetch(`http://127.0.0.1:8000/api/patient/${patientid}/?format=json`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "first_name":String(mewPatient.first_name).trim(),
            "last_name":String(mewPatient.last_name).trim(),
            "dni":parseInt(mewPatient.dni),
            "adress":String(mewPatient.adress).trim(),
            "phone":parseInt(mewPatient.phone),
            "sex":String(mewPatient.sex).trim(),
            "date_of_birth":String(mewPatient.date_of_birth),
            "date_of_register":String(mewPatient.date_of_register),
            "date_of_modification":String(mewPatient.date_of_modification),
            "register_user":String(mewPatient.register_user).trim(),
            "modified_user":String(mewPatient.modified_user).trim(),
            "active":String(mewPatient.active)
        })
    })
};

export const getPatient = async (patientid)=>{
    return await fetch(`http://127.0.0.1:8000/api/patient/${patientid}/?format=json`)
};

export const deletePatient = async (patientid)=>{
    return await fetch(`http://127.0.0.1:8000/api/patient/${patientid}/?format=json`,{
        method: 'DELETE',
    })
};