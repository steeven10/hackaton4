import { PatientItem } from "./patient_item";

import { useEffect, useState } from "react";
import axios from "axios";

export function Patient_list () {
    const [patient,setPatient] = useState(null)

    const listPatient = async (req, res) => {
        axios.get('http://127.0.0.1:8000/api/patient/?format=json').then((respuesta)=>{
            setPatient(respuesta.data)
        }
        )
    }

    useEffect(() =>{
        listPatient();

    },[])
    return (<div>
                <h1 className="text-center my-3">Patient</h1>
    <div className="row container">
        {patient ? patient.results.map((elemento)=>{
        return <PatientItem
        key = {elemento.id}
        {...elemento}
        listPatient={listPatient}/>}) : "patients..."}
    </div>
    </div>

    )
}