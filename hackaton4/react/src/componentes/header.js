

export function Header() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark" style={{backgroundColor: 'black' }}>
            <div className="container-fluid">
                <a className="navbar-brand" href="/">Hospital</a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/">Home</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/patient" role="button" >Patient</a>
                            <ul className="dropdown-menu">
                                <li><a class="dropdown-item" href="/patient">Patient list</a></li>
                                <li><a class="dropdown-item" href="/patient_form">Patient form</a></li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link active" aria-current="page" href="/medical">Medical</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/medical_speciality" role="button" >Medical Speciality</a>
                            <ul className="dropdown-menu">
                                <li><a class="dropdown-item" href="/medical_speciality">Medical Speciality list</a></li>
                                <li><a class="dropdown-item" href="/medical_speciality_form">Medical Speciality form</a></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/speciality">Specialist</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/shedule">Shedule</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/date">Date</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}