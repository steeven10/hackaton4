

import { useEffect, useState } from "react";
import axios from "axios";
import { SpecialitytItem } from "./speciality_item";

export function Speciality_list () {
    const [speciality,setSpeciality] = useState(null)

    useEffect(() =>{
        axios.get('http://127.0.0.1:8000/api/speciality/?format=json').then((respuesta)=>{
            setSpeciality(respuesta.data)
        }
        )

    },[])
    return (
    <div className="row">
        {speciality ? speciality.map((elemento)=>{
        return <SpecialitytItem
        key = {elemento.id}
        {...elemento}/>}) : "patients..."}
    </div>
    )
}