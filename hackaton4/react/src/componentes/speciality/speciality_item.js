import {Link} from "react-router-dom"

function colorEstiloCirculo(color = 'green') {
    const estiloDelCirculo = {
        width: '10px',
        height: '10px',
        display: "inline-block",
        backgroundColor: color,
        borderRadius: '50%',
        marginRight: "5px"
    }
    return estiloDelCirculo
}

export function SpecialitytItem({
                                   id,
                                   name,
                                   description,
                                   date_of_register,
                                   date_of_modification,
                                   register_user,
                                   modified_user,
                                   active
                               }) {
    let color = 'green';
    if (active === true) {
        color = 'green'
    }
    if (active === false) {
        color = 'red'
    }
    return (
        <div className="mx-5">
            <h1 className="text-center my-3">Medical</h1>
            <div className="py-3 col-4 ">
            <Link className="navbar-brand" style={{color: 'initial'}} to={`/patient/${id}`}>
                <div className="card mb-3" style={{maxWidth: "540px"}}>
                    <div className="row g-0 nabvar-primary bg-dark" >
                        <div className="col-md-10 ">
                            <div className="card-body">
                                <p style={{color: 'white'}}><span style={colorEstiloCirculo(color)}
                                ></span>
                                    {active}  {name}
                                </p>
                                <p className="card-text mb-0"><small className="text-muted">Description</small></p>
                                <p style={{color: 'white'}}> {description}</p> 
                                <p className="card-text mb-0"><small className="text-muted">Date of register</small></p>
                                <p style={{color: 'white'}}> {date_of_register}</p>
                                <p className="card-text mb-0"><small className="text-muted">Date of modification</small>
                                </p>
                                <p style={{color: 'white'}}> {date_of_modification}</p>
                                <p className="card-text mb-0"><small className="text-muted">User register</small></p>
                                <p style={{color: 'white'}}> {register_user}</p>
                                <p className="card-text mb-0"><small className="text-muted">User modified</small></p>
                                <p style={{color: 'white'}}> {modified_user}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>

        </div>
        </div>
        

    )
}